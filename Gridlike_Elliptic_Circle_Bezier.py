import numpy as np
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
from matplotlib.patches import Polygon
from matplotlib import collections  as mc
from collections import deque
from matplotlib.artist import Artist
from matplotlib.widgets import Slider
from threading import Timer
import random

def dist(x, y):
    """
    Return the distance between two points.
    """
    d = x - y
    return np.sqrt(np.dot(d, d))

def dist_point_to_segment(p, s0, s1):
    """
    Get the distance of a point to a segment.
      *p*, *s0*, *s1* are *xy* sequences
    This algorithm from
    http://geomalgorithms.com/a02-_lines.html
    """
    v = s1 - s0
    w = p - s0
    c1 = np.dot(w, v)
    if c1 <= 0:
        return dist(p, s0)

    c2 = np.dot(v, v)
    if c2 <= c1:
        return dist(p, s1)

    b = c1 / c2
    pb = s0 + b * v
    return dist(p, pb)

class PolygonInteractor(object):
    showverts = True
    epsilon = 7  # max pixel distance to count as a vertex hit

    def __init__(self, ax, poly, visible=False):
        if poly.figure is None:
            raise RuntimeError('You must first add the polygon to a figure '
                               'or canvas before defining the interactor')
        self.ax = ax
        canvas = poly.figure.canvas
        self.poly = poly
        self.poly.set_visible(visible)

        x, y = zip(*self.poly.xy)
        self.line = Line2D(x, y, ls="", marker='o', markerfacecolor='r', animated=True)
        self.ax.add_line(self.line)

        self.cid = self.poly.add_callback(self.poly_changed)
        self._ind = None  # the active vert
        self.playtoggle = False
        canvas.mpl_connect('draw_event', self.draw_callback)
        canvas.mpl_connect('button_press_event', self.button_press_callback)
        canvas.mpl_connect('button_release_event', self.button_release_callback)
        canvas.mpl_connect('motion_notify_event', self.motion_notify_callback)
        canvas.mpl_connect('key_press_event', self.key_press_callback)
        self.canvas = canvas

    def draw_callback(self, event):
        self.background = self.canvas.copy_from_bbox(self.ax.bbox)
        self.ax.draw_artist(self.poly)
        self.ax.draw_artist(self.line)
        # do not need to blit here, this will fire before the screen is updated

    def poly_changed(self, poly):
        'this method is called whenever the polygon object is called'
        # only copy the artist props to the line (except visibility)
        vis = self.line.get_visible()
        Artist.update_from(self.line, poly)
        self.line.set_visible(vis)  # don't use the poly visibility state


    def get_ind_under_point(self, event):
        'get the index of the vertex under point if within epsilon tolerance'

        # display coords
        xy = np.asarray(self.poly.xy)
        xyt = self.poly.get_transform().transform(xy)
        xt, yt = xyt[:, 0], xyt[:, 1]
        d = np.hypot(xt - event.x, yt - event.y)
        indseq, = np.nonzero(d == d.min())
        ind = indseq[0]

        if d[ind] >= self.epsilon:
            ind = None

        return ind

    def button_press_callback(self, event):
        'whenever a mouse button is pressed'
        if not self.showverts:
            return
        if event.inaxes is None:
            return
        if event.button != 1:
            return
        self._ind = self.get_ind_under_point(event)

    def button_release_callback(self, event):
        'whenever a mouse button is released'
        if not self.showverts:
            return
        if event.button != 1:
            return
        self._ind = None

        self.canvas.draw_idle()

    def motion_notify_callback(self, event):
        'on mouse movement'
        if not self.showverts:
            return
        if self._ind is None:
            return
        if event.inaxes is None:
            return
        if event.button != 1:
            return
        x, y = event.xdata, event.ydata
        self.poly.xy[self._ind] = x, y

        self.draw_update()
        update(1)
        self.canvas.draw_idle()
    
    def on_timer(self):
        alpha.val += 0.01
        if alpha.val >= 1:
            alpha.val -= 1
        alpha.set_val(alpha.val)
        if self.playtoggle:
            player = Timer(0.02, self.on_timer)
            player.start()


    def key_press_callback(self, event):#
        global playtoggle
        'whenever a key is pressed'
        if not event.inaxes:
            return

        if event.key == 'd':
            ind = self.get_ind_under_point(event)
            if ind is not None:
                self.poly.xy = np.delete(self.poly.xy,
                                         ind, axis=0)
                self.line.set_data(zip(*self.poly.xy))
        elif event.key == 'i':
            xys = self.poly.get_transform().transform(self.poly.xy)
            p = event.x, event.y  # display coords
            for i in range(len(xys) - 1):
                s0 = xys[i]
                s1 = xys[i + 1]
                d = dist_point_to_segment(p, s0, s1)
                if d <= self.epsilon:
                    self.poly.xy = np.insert(self.poly.xy, i+1,[event.xdata, event.ydata],axis=0)
                    self.line.set_data(zip(*self.poly.xy))
                    break
        elif event.key == ' ':
            self.playtoggle = not self.playtoggle
            if self.playtoggle:
                self.on_timer()
        update(1)

    def draw_update(self):
        self.line.set_data(zip(*self.poly.xy))

        self.canvas.restore_region(self.background)
        self.ax.draw_artist(self.poly)
        self.ax.draw_artist(self.line)
        self.canvas.blit(self.ax.bbox)
        update(1)

def update(val):
    global Iterations
    x, y = poly.xy[:].T
    Quadrilateral = np.zeros((x.shape[0], 2))
    Quadrilateral[:,0], Quadrilateral[:,1] = x, y
    lc.set_segments(create_Points(Quadrilateral, max(Iterations, 0)))
    fig.canvas.draw()

def create_Points(Original_Quadrilateral: np.ndarray, Max_IT: int):
    global lines, Switch
    lines = []
    To_do = deque()
    To_do.append([Original_Quadrilateral, 0])
    if alpha.val == 0 or alpha.val == 1:
        Max_IT = 1

    while len(To_do) != 0:
        [Quad, Iteration] = To_do.popleft()
        if Iteration > Max_IT:
            if Switch:
                for i in range(Quad.shape[0]-1):
                    get_vector(Quad[i+1], Quad[i],l=True)
                get_vector(Quad[0], Quad[-1],l=True)  
            continue

        if not Switch:
            for i in range(Quad.shape[0]-1):
                get_vector(Quad[i+1], Quad[i],l=True)
            get_vector(Quad[0], Quad[-1],l=True)

        New = add_Quad(Quad, alpha.val)
        To_do.append([New, Iteration + 1])
    return (lines)

def add_Quad(Quad, val):
    New = np.zeros((Quad.shape[0],Quad.shape[1])) # (Quad.shape[0]-1,Quad.shape[1]) for normal bezier
    for i in range(Quad.shape[0] - 1):
        New[i] = get_point(Quad[i+1],Quad[i],val)
    New[-1] = get_point(Quad[0],Quad[-1],val)
    return New
    
def get_point(end, start, val):
    vector = get_vector(end, start)

    new_vector = start + vector*val
    for i in range(2):
        if new_vector[i]>5:
            new_vector[i] -= 10
        elif new_vector[i]<-5:
            new_vector[i] += 10
        
    return new_vector

def get_vector(end, start, l:bool = False):
    vector = end-start # euclidian
    end = np.add(end,5)
    start = np.add(start,5)
    mod = False
    for i in range(2):
        if abs(end[i]-start[i]) > min(10-end[i],end[i])+min(10-start[i],start[i]):
            mod = True

            vector[i] = min(10-end[i],end[i]) + min(10-start[i],start[i])

            if end[i]-start[i]>0:
                vector[i] = -vector[i]

    end = np.add(end,-5)
    start = np.add(start,-5)

    if l:  
        global lines   
        lines.append([(start[0], start[1]), (start[0]+vector[0], start[1]+vector[1])])
        if mod:
            lines.append([(end[0], end[1]), (end[0]-vector[0], end[1]-vector[1])])
    else:
        return vector

def key_press(event):
    if not event.inaxes:
        return
    global Iterations, Switch
    if event.key == "J":
        Iterations += 1
    elif event.key == "j":
        Iterations-= 1
    elif event.key == "b" or event.key == "B":
        Switch = not Switch

    update(1)

fig, ax1 = plt.subplots()
ax1.set_xlim([-5,5])
ax1.set_ylim([-5,5])

Iterations = 5
Switch = False

List_quadrilateral = [[1,1],[-1,1],[-1,-1],[1,-1]]
Sides = [[0,1],[-1,0],[0,-1],[1,0]]
Quadrilateral = np.array(List_quadrilateral)

Grid_Size = (20,20)

lines = []
lc = mc.LineCollection(lines, linewidths=1, color = 'black')
ax1.add_collection(lc)

poly = Polygon(list(zip(Quadrilateral[:,0], Quadrilateral[:,1])), animated=True, closed = False)
ax1.add_patch(poly)
Point = PolygonInteractor(ax1, poly, visible=False)

Slider_ax = plt.axes([0.1, 0.02, 0.8, 0.03]) # set slider coordinates
alpha = Slider(ax=Slider_ax, label=r"$\alpha$", valmin=0, valmax=1, valinit=0)
alpha.on_changed(update)
fig.canvas.mpl_connect('key_press_event', key_press)

update(1)

plt.show()
Point.playtoggle = False